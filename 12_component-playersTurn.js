'use strict';

class Pexeso extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
        	started: false,
          	random: null,
          	players: []
        };
        this.name = [];
    }

    generateRandomPexeso() {
      const array = [
        {stamp:1, color:'blue'}, 
        {stamp:2, color:'red'}, 
        {stamp:3, color:'green'}, 
        {stamp:4, color:'orange'}, 
        {stamp:5, color:'violet'}, 
        {stamp:6, color:'brown'}, 
        {stamp:7, color:'gray'}, 
        {stamp:8, color:'black'}, 
        {stamp:9, color:'slateblue'}, 
        {stamp:10, color:'aqua'}, 
        {stamp:11, color:'teal'}, 
        {stamp:12, color:'lime'}, 
        {stamp:13, color:'olive'}, 
        {stamp:14, color:'navy'}, 
        {stamp:15, color:'fuchsia'}, 
        {stamp:16, color:'maroon'}, 
        {stamp:17, color:'salmon'}, 
        {stamp:18, color:'purple'}];

      const doubleArray = array.concat(JSON.parse(JSON.stringify(array)));
      doubleArray.sort(() => Math.random() - 0.5);

      return doubleArray;
    };

    play() {
      this.setState({
        started: true,
        players: this.setPlayer(),
        random: this.generateRandomPexeso()
      })
    }

    setPlayer() {
      var players = [];
      this.name.map((n, i) => {
        if(n.value !== "") {
          players.push({'name': n.value, 'score': 0});
        }
      });

      return players;
    }

    render() {
        return (
            <div className="board">
              <h1>Pexeso</h1>
               {(this.state.started === false) ? (
                <div className="players">
                	<label>Jména hráčů:</label>
                  {Array.apply(null, Array(4)).map((x, i) => {
                    return (
                      <input key={i} type="text" ref={(name) => { this.name[i] = name }} />
                    );
                  })}
                  <input type="submit" value="Play" onClick={() => this.play()} />
                </div>
              ) : (
                <Board random={this.state.random} players={this.state.players} newGame={() => this.setState({ started: false })} />
              )}
            </div>
        );
    }
}

class Board extends React.Component {

  constructor(props) {
      super(props);
      this.state = {
        players: this.setTurn(props.players),
        squares:props.random,
        firstClick: null
      };
  }

  setOpenSquare(squares, key) {
    squares[key].open = true;

    return squares;
  }

  setWinSquare(squares) {
    squares.map((s, i) => {
        if(s.stamp == this.state.firstClick) {
          s.win = true;
        }
    });

    return squares;
  }

  setCloseSquaresWithDelay(squares) {
    setTimeout(() => {
      squares.map((s, i) => {
        s.open = false;
      });
      this.setState({
        squares: squares,
        pause: false
      });
    }, 2000)
  }

  setTurn(players) {
    var activeTurn = Object.keys(players).filter(key => players[key].turn == true);
    var newTurn = (activeTurn.length > 0 && activeTurn < activeTurn.length) ? activeTurn[0] + 1 : 0;
    
    players.map((p, key) => {
      p.turn = (key == newTurn ? true : false);
    });
      
    return players;
  }

  setScore(players) {
    var activeTurn = Object.keys(players).filter(key => players[key].turn == true)[0];
    players[activeTurn].score = players[activeTurn].score + 1;
      
    return players;
  }

  handleClick(square, key) { 
  	if(square.open != true && square.win != true && this.state.pause != true) {
  		var squares = this.setOpenSquare(this.state.squares, key);
	    if(this.state.firstClick === null) {
	        console.log('click', square, key);
	        this.setState({
	          firstClick: square.stamp,
	          squares: squares
	        });
	    } else {
	        this.handleSecondClick(square);
	    }
	}
  }

  handleSecondClick(square) {
  	var squares = (square.stamp === this.state.firstClick) ? this.setWinSquare(this.state.squares) : this.state.squares;
  	var players = (square.stamp === this.state.firstClick) ? this.setScore(this.state.players) : this.setTurn(this.state.players);

    this.setState({
      firstClick: null,
      squares: squares,
      players: players,
      pause: true
    });

    this.setCloseSquaresWithDelay(this.state.squares);
  }

  render() {
    return (
      <div className="pexeso">
        <div className="info-board">
          <h2>Board</h2>
          <hr />
          <button onClick={() => this.props.newGame()}>Nová hra</button>
        </div>
        <div className="game">
          {this.state.squares.map((square, key) =>
              <Square key={key} value={square} clickSquare={() => this.handleClick(square, key)} />
          )}
        </div>
      </div>
    );
  }
}

class Square extends React.Component {
  constructor(props) {
    super(props);
  }

  render() {
    return (
      <button className={'square' + (this.props.value.open == true || this.props.value.win == true ? ' open' : '')} onClick={() => this.props.clickSquare()} style={{color: this.props.value.color}}>
        <span>
          {this.props.value.stamp}
        </span>
      </button>
    );
  }
}

if (document.getElementById('pexeso')!==null) {
  ReactDOM.render(
      <Pexeso />
      ,document.getElementById('pexeso')
  );
}
