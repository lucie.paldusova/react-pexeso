'use strict';

class Pexeso extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
          random: this.generateRandomPexeso(),
        };
    }

    generateRandomPexeso() {
      const array = [
        {stamp:1, color:'blue'}, 
        {stamp:2, color:'red'}, 
        {stamp:3, color:'green'}, 
        {stamp:4, color:'orange'}, 
        {stamp:5, color:'violet'}, 
        {stamp:6, color:'brown'}, 
        {stamp:7, color:'gray'}, 
        {stamp:8, color:'black'}, 
        {stamp:9, color:'slateblue'}, 
        {stamp:10, color:'aqua'}, 
        {stamp:11, color:'teal'}, 
        {stamp:12, color:'lime'}, 
        {stamp:13, color:'olive'}, 
        {stamp:14, color:'navy'}, 
        {stamp:15, color:'fuchsia'}, 
        {stamp:16, color:'maroon'}, 
        {stamp:17, color:'salmon'}, 
        {stamp:18, color:'purple'}];

      const doubleArray = array.concat(JSON.parse(JSON.stringify(array)));
      doubleArray.sort(() => Math.random() - 0.5);

      return doubleArray;
    };

    render() {
        return (
            <div className="board">
              <h1>Pexeso</h1>
              <Board random={this.state.random} />
            </div>
        );
    }
}

class Board extends React.Component {

  constructor(props) {
      super(props);
      this.state = {
        squares:props.random,
        firstClick: null
      };
  }

  setOpenSquare(squares, key) {
    squares[key].open = true;

    return squares;
  }

  setWinSquare(squares) {
    squares.map((s, i) => {
        if(s.stamp == this.state.firstClick) {
          s.win = true;
        }
    });

    return squares;
  }

  setCloseSquaresWithDelay(squares) {
    setTimeout(() => {
      squares.map((s, i) => {
        s.open = false;
      });
      this.setState({
        squares: squares,
        pause: false
      });
    }, 2000)
  }

  handleClick(square, key) { 
  	if(square.open != true && square.win != true && this.state.pause != true) {
  		var squares = this.setOpenSquare(this.state.squares, key);
	    if(this.state.firstClick === null) {
	        console.log('click', square, key);
	        this.setState({
	          firstClick: square.stamp,
	          squares: squares
	        });
	    } else {
	        this.handleSecondClick(square);
	    }
	}
  }

  handleSecondClick(square) {
  	var squares = (square.stamp === this.state.firstClick) ? this.setWinSquare(this.state.squares) : this.state.squares;

    this.setState({
      firstClick: null,
      squares: squares,
      pause: true
    });

    this.setCloseSquaresWithDelay(this.state.squares);
  }

  render() {
    return (
      <div className="pexeso">
        <div className="info-board">
          <h2>Board</h2>
          <hr />
        </div>
        <div className="game">
          {this.state.squares.map((square, key) =>
              <Square key={key} value={square} clickSquare={() => this.handleClick(square, key)} />
          )}
        </div>
      </div>
    );
  }
}

class Square extends React.Component {
  constructor(props) {
    super(props);
  }

  render() {
    return (
      <button className={'square' + (this.props.value.open == true || this.props.value.win == true ? ' open' : '')} onClick={() => this.props.clickSquare()} style={{color: this.props.value.color}}>
        <span>
          {this.props.value.stamp}
        </span>
      </button>
    );
  }
}

if (document.getElementById('pexeso')!==null) {
  ReactDOM.render(
      <Pexeso />
      ,document.getElementById('pexeso')
  );
}
