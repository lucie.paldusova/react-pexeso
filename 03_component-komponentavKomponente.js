'use strict';

class Pexeso extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
        };
    }

    render() {
        return (
            <div className="board">
              <h1>Pexeso</h1>
              <Board />
            </div>
        );
    }
}

class Board extends React.Component {

  constructor(props) {
      super(props);
  }

  render() {
    return (
      <div className="pexeso">
        <div className="info-board">
          <h2>Board</h2>
          <hr />
        </div>
      </div>
    );
  }
}

if (document.getElementById('pexeso')!==null) {
  ReactDOM.render(
      <Pexeso />
      ,document.getElementById('pexeso')
  );
}
